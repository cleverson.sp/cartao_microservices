package br.com.Imersao.Cliente.controllers;

import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import br.com.Imersao.Cliente.models.Cliente;
import br.com.Imersao.Cliente.services.ClienteService;
//import br.itau.com.cartoes.dtos.Login;
//import br.itau.com.cartoes.dtos.RespostaLogin;
//import br.itau.com.cartoes.security.JwtTokenProvider;

@RestController
public class ClienteController {
  @Autowired
  private ClienteService clienteService;
  
//  @Autowired
//  private JwtTokenProvider tokenProvider;
  
  @PostMapping("/cliente")
  @ResponseStatus(code = HttpStatus.CREATED)
  public Cliente criar(@Valid @RequestBody Cliente cliente) {
    return clienteService.criar(cliente);
  }
  
  @GetMapping("/cliente/{id}")
  public Optional<Cliente> criar(@PathVariable int id) {
    return clienteService.buscar(id);
  }
  
//  @PostMapping("/login")
//  public RespostaLogin login(@Valid @RequestBody Login login) {
//    Optional<Cliente> optional = clienteService.login(login);
//    
//    if(!optional.isPresent()) {
//      throw new ResponseStatusException(HttpStatus.FORBIDDEN);
//    }
//    
//    Cliente cliente = optional.get();
//    String idUsuario = String.valueOf(cliente.getId());
////    String token = tokenProvider.criarToken(idUsuario);
//    
//    RespostaLogin resposta = new RespostaLogin();
////    resposta.setCliente(cliente);
////    resposta.setToken(token);
////    
//    return resposta;
//  }
}
