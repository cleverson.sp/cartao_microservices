package br.com.Imersao.Cliente.dtos;

import br.com.Imersao.Cliente.models.Cliente;

public class RespostaLogin {
  private Cliente cliente;
  private String token;
  
  public Cliente getCliente() {
    return cliente;
  }
  
  public void setCliente(Cliente cliente) {
    this.cliente = cliente;
  }
  
  public String getToken() {
    return token;
  }
  
  public void setToken(String token) {
    this.token = token;
  }
}
