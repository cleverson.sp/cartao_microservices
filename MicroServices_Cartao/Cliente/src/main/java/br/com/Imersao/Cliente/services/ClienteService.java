package br.com.Imersao.Cliente.services;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.Imersao.Cliente.exceptions.ValidacaoException;
import br.com.Imersao.Cliente.models.Cliente;
import br.com.Imersao.Cliente.repositories.ClienteRepository;

@Service
public class ClienteService {
  @Autowired
  private ClienteRepository clienteRepository;
  
//  @Autowired
//  private BCryptPasswordEncoder passwordEncoder;
  
  public Cliente criar(Cliente cliente) {
    if(cliente.getId() != 0) {
      throw new ValidacaoException("id", "Id não pode ser definido ao criar um cliente");
    }
    
//    String senhaCriptografada = passwordEncoder.encode(cliente.getSenha());
//    cliente.setSenha(senhaCriptografada);
    
    return clienteRepository.save(cliente);
  }
  
  public Optional<Cliente> buscar(int id) {
    return clienteRepository.findById(id);
  }
  
//  public Optional<Cliente> login(Login login){
//    Optional<Cliente> optional = clienteRepository.findByCpf(login.getCpf());
//    
//    if(!optional.isPresent()) {
//      return optional;
//    }
//    
//    Cliente cliente = optional.get();
//    
//    if(passwordEncoder.matches(login.getSenha(), cliente.getSenha())) {
//      return optional;
//    }
//    
//    return Optional.empty();
//  }
}
