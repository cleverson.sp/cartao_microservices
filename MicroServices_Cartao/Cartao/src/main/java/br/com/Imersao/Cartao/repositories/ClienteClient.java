package br.com.Imersao.Cartao.repositories;

import java.util.Optional;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import br.com.Imersao.Cartao.dtos.Cliente;

@FeignClient(name = "Cliente")
public interface ClienteClient {
	
	@GetMapping("/cliente/{id}")
	public Optional<Cliente> buscar(@PathVariable int id);
	

}


