package br.com.Imersao.Lancamento.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.Imersao.Lancamento.models.Fatura;
import br.com.Imersao.Lancamento.services.FaturaService;

@RestController
@RequestMapping("/fatura")
public class FaturaController {
	
	@Autowired
	private FaturaService faturaService;

	@GetMapping("/{numero}")
	public Fatura buscar(@PathVariable String numero) {
		return faturaService.buscar(numero);
	}
	
}
