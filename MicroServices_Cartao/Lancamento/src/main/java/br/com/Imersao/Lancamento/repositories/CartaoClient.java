package br.com.Imersao.Lancamento.repositories;

import java.util.Optional;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import br.com.Imersao.Lancamento.dtos.Cartao;

@FeignClient(name = "Cartao")
public interface CartaoClient{
	
	@GetMapping("/cartao/{numCartao}")
	public Optional<Cartao> buscar(@PathVariable String numCartao);

}
